import {RegisterComponent} from './register';
import {Reg1Component} from './reg_1';
import {Reg2Component} from './reg_2';
import {Reg3Component} from './reg_3';
import {Reg4Component} from './reg_4';
import {Reg5Component} from './reg_5';
import {DashboardComponent} from './dashboard';
import {FinancialComponent} from './financial';
import {GraphsComponent} from './graphs';
import {LoginComponent} from './login';
export const UI_DECLARATIONS = [
  RegisterComponent,
  Reg1Component,
  Reg2Component,
  Reg3Component,
  Reg4Component,
  Reg5Component,
  DashboardComponent,
  FinancialComponent,
  GraphsComponent,
  LoginComponent
];