import {Component} from '@angular/core';
import template from './dashboard.html';
@Component({
    selector:'dashboard',
    template
})
export class DashboardComponent{}