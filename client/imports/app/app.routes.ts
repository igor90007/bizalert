import {Route} from '@angular/router';
import {Meteor} from 'meteor/meteor';
import {RegisterComponent} from './ui/register';
import {Reg1Component} from './ui/reg_1';
import {Reg2Component} from './ui/reg_2';
import {Reg3Component} from './ui/reg_3';
import {Reg4Component} from './ui/reg_4';
import {Reg5Component} from './ui/reg_5';
import {DashboardComponent} from './ui/dashboard';
import {FinancialComponent} from './ui/financial';
import {GraphsComponent} from './ui/graphs';
import {LoginComponent} from './ui/login';
export const routes: Route[] = [
    {path:'',component:RegisterComponent},
    {path:'register',component:RegisterComponent},
    {path:'reg_1',component:Reg1Component,canActivate:['canActivateForLoggedIn']},
    {path:'reg_2',component:Reg2Component,canActivate:['canActivateForLoggedIn']},
    {path:'reg_3',component:Reg3Component,canActivate:['canActivateForLoggedIn']},
    {path:'reg_4',component:Reg4Component,canActivate:['canActivateForLoggedIn']},
    {path:'reg_5',component:Reg5Component,canActivate:['canActivateForLoggedIn']},
    {path:'dashboard',component:DashboardComponent,canActivate:['canActivateForLoggedIn']},
    {path:'financial',component:FinancialComponent,canActivate:['canActivateForLoggedIn']},
    {path:'graphs',component:GraphsComponent,canActivate:['canActivateForLoggedIn']},
    {path:'login',component:LoginComponent,canActivate:['canActivateForLoggedIn']}
];
export const ROUTES_PROVIDERS=[{
    provide:'canActivateForLoggedIn',
    useValue:()=>!!Meteor.userId()
}];