import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from "./app.component";
import {RouterModule} from '@angular/router';
import {AccountsModule} from 'angular2-meteor-accounts-ui';
import {routes,ROUTES_PROVIDERS} from './app.routes';
import {MaterialModule} from "@angular/material";
import {UI_DECLARATIONS} from "./ui/index";
let moduleDefinition;
moduleDefinition={
    imports:[
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot(routes),
      AccountsModule,
      MaterialModule.forRoot()
    ],
    declarations:[
      AppComponent,
      ...UI_DECLARATIONS
    ],
    providers:[
      ...ROUTES_PROVIDERS
    ],
    bootstrap:[
      AppComponent
    ]
}
@NgModule(moduleDefinition)
export class AppModule{}