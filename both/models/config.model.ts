import { CollectionObject } from './collection-object.model';
export interface Config extends CollectionObject{
  name?: string;
}
