import {CollectionObject} from './collection-object.model';
export interface Tasks extends CollectionObject{
  name?: string;
}
