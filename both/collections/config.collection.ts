import {MongoObservable} from "meteor-rxjs";
import {Config} from "../models/config.model";

export const ConfigCollection = new MongoObservable.Collection<Config>("config");
