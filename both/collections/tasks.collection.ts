import {MongoObservable} from "meteor-rxjs";
import {Tasks} from "../models/tasks.model";
export const TasksCollection = new MongoObservable.Collection<Tasks>("tasks");